# \autor kyb
# \description
# Создать VPN-подключение и задачу на прописывание маршрутов.
#--------------------------------------------------------------------


[CmdletBinding()]
Param(
	# Name of VPN connection
	[Parameter(Mandatory=$True,ValueFromPipeline=$True,Position=0, ParameterSetName="WithParams")]
	[string]$vpn_name,

    # Server URL or IP
    [Parameter(Mandatory=$True,ValueFromPipeline=$True,Position=1, ParameterSetName="WithParams")]
    [string]$vpn_server,

	# Целевые адреса в виде адрес/маска, например 192.168.72.0/24
	[Parameter(Mandatory=$True,ValueFromPipeline=$True,Position=2, ParameterSetName="WithParams")]
	[string[]]$target_nets,

    # Alternative set of parameters (No porameters
	#
	[Parameter(Mandatory=$False,ValueFromPipeline=$True,Position=0, ParameterSetName="NoParams")]
	[string]$none
)

switch ($PsCmdlet.ParameterSetName)
{
    "WithParams"  {
		"# VPN Connection provisioning. `n" +`
		"# Created automatically from parameters passed to vpn-create.ps1" +`
		'$vpn_name = ' + $vpn_name + "`n" +`
		'$vpn_server = ' + $vpn_server + "`n"  +`
		'$target_nets = ' + $target_nets + "`n"  >`
		vpn-params.ps1
	}
    "NoParams"    {
		Write-Host "No params passed. Using params from vpn-params.ps1"
		. $PSScriptRoot/vpn-params.ps1
		break
	}
}

Import-Module $PSScriptRoot/pressAnyKeyToExit

#------------------------------------------------------------
# VPN Connection look-up to check any previous installations
#------------------------------------------------------------
$vpnConnections = Get-VpnConnection #-AllUserConnection

if($vpnConnections.Name -eq $vpn_name)
{
    Write-Host $vpn_name connection is already configured on your system. -ForegroundColor Yellow
    Write-Host "If you wish to reinstall, please uninstall the connection and then attemp again." -ForegroundColor Yellow
    do{
        $x = read-host "Do you wish to uninstall $vpn_name (Y or N)"
    	if( $x[0] -eq "Y" ){
            rasdial $vpn_name /disconnect;
            Remove-VpnConnection -Name $vpn_name -Force
            break
        }elseif( $x[0] -eq "N" ){
            exit
        }
    }while( $True )
}

Write-Host Installing $vpn_name connection.
try
{
    ## Create the VPN connection "My VPN" with the EAP configuration XML generated above
    Add-VpnConnection -Name $vpn_name -ServerAddress $vpn_server -EncryptionLevel Required -TunnelType PPTP #-AuthenticationMethod MSChapV2 -SplitTunneling $True

	## -SplitTunneling $True - Don't use as Internet adapter. Make it True if you have internet from another adapter.
	## -RememberCredential $True - Remeber login/password after first enter
	Set-VpnConnection -Name $vpn_name -SplitTunneling $True -RememberCredential $True
}
catch
{
    Write-Host "Error in connection setup!" -ForegroundColor Red -BackgroundColor Black
    Write-Host $_.Exception.Message
    throw
}

Write-Host $vpn_name connection is ready for use.


##------------------------------------------------------------
## Schedule task on VPN connection established
##------------------------------------------------------------
[xml]$task_xml = Get-Content $PSScriptRoot\vpn-conn-update-task-template.xml
$task_xml.Task.RegistrationInfo.URI = "\$vpn_name add routes on connect"
$task_xml.Task.Triggers.EventTrigger.Subscription = '<QueryList><Query Id="0" Path="Application"><Select Path="Application">*[System[(Level=4 or Level=0) and (EventID=20225)]] and *[EventData[Data="'+$vpn_name+'"]]</Select></Query></QueryList>'
$task_xml.Task.Actions.Exec.Command = "Powershell.exe"
$task_xml.Task.Actions.Exec.Arguments = "-noProfile -NonInteractive -WindowStyle Hidden `"$PSScriptRoot\vpn-route.ps1 $vpn_name -TargetNets $target_nets`""
$task_xml.Task.Actions.Exec.WorkingDirectory = $PSScriptRoot
$task_xml.Save("$PSScriptRoot\vpn-conn-update-task.xml")

Start-Process schtasks -ArgumentList "/create /tn `"$vpn_name add routes on connect`" /F /XML `"$PSScriptRoot\vpn-conn-update-task.xml`"" -Verb RunAs -Wait

Write-Host "Add route task scheduled." -foreground Yellow
pressAnyKeyToExit
