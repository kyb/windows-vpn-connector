# \autor kyb
# \description
# Назначить маршруты на заданные целевые адреса через заданный по имени интерфейс.
# Если вы находитесь в одной из сетей, к которой извне подключаетесь по VPN этот
# скрипт не зарубит локалку, а пропишет маршруты только для тех подсетей, которых 
# ещё нет в списке.
#--------------------------------------------------------------------


[CmdletBinding()]
Param(
	# Имя адаптера
	[Parameter(Mandatory=$True,ValueFromPipeline=$True,Position=1)]
	[string]$AdapterName,

	# Целевые адреса в виде адрес/маска, например 192.168.72.0/24
	[Parameter(Mandatory=$True,ValueFromPipeline=$True)]
	[string[]]$TargetNets
)

Import-Module $PSScriptRoot/pressAnyKeyToExit

. vpn-params.ps1
if( !$PSBoundParameters.ContainsKey('AdapterName') ){
	$AdapterName = $vpn_name
}
if( !$PSBoundParameters.ContainsKey('TargetNets') ){
	$TargetNets = $target_nets
}

write-host Remove routes which are binded to the specified interface only
# Find interface
$ifw =  Get-NetIPInterface | where-object { $_.InterfaceAlias -eq $AdapterName }
if(!$ifw){
	write-host "Network interface not found. " -NoNewLine;
	pressAnyKeyToExit;
}

# Remove routes which are binded to the specified interface only
foreach($target in $TargetNets) {
	$hasRoute = Get-NetRoute -AssociatedIPInterface $ifw | where-object { $_.DestinationPrefix -match $target }
	if($hasRoute) {
		"Remove-NetRoute $target"
		Remove-NetRoute $target -Confirm:$false
	}
}
write-host Done.

# Add routes, if target addresses are NOT binded to other interfaces. No duplicates
write-host "Add routes which are not exist yet"
foreach($target in $TargetNets) {
	$hasRoute = Get-NetRoute | where-object { $_.DestinationPrefix -match $target }
	if( !$hasRoute ){
		"New-NetRoute -DestinationPrefix $target -InterfaceAlias $AdapterName -RouteMetric 1 -PolicyStore ActiveStore"
		New-NetRoute -DestinationPrefix $target -InterfaceAlias $AdapterName -RouteMetric 1 -PolicyStore ActiveStore
	}
}
write-host Done.

#write-host "Script Done. " -NoNewLine; pressAnyKeyToExit;
