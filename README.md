There is [russian](README.rus.md) version.

Windows VPN connector
=======================
This set of scripts offered to solve the problem with the automatic addition of routes after connection to a virtual private network (VPN) established. Invented as opposed to adding a permanent route. It is important for portable computers, especially if the computers themselves may directly involved in the target network.  
This set of PowerShell scripts allows to create VPN connection with next useful features:

1. automatically register routes to removed networks
2. will not disconnect you from internet
3. they may be reconfigured to your taste


## Step by step

0. Сlone this repository `cd my/folder/for/vpn && git clone https://qyw@bitbucket.org/qyw/windows-vpn-connector-kosmos6.git` or [download](https://bitbucket.org/qyw/windows-vpn-connector-kosmos6/downloads) zip. Do **not** delete the folder after installation. It contains files which will be executed on connection established.
1. Edit `vpn-params.ps1` to specify `$vpn_name`, `$vpn_server` (URL or IP), `$target_nets` in ip/mask format.
2. Run script via right click on `vpn-create.ps1` and  ***execute in PowerShell*** . It may require admin rights to execute, so run PowerShell with admin rights and drag file to shell window.


## Description of files

`.ps1` is PowerShell script.

1. `vpn-create.ps1` - Main file to run. It creates VPN connection and schedules task, which will be run after connection established. Can accept cmd line arguments or take params from `vpn-params.ps1`.
2. `vpn-route.ps1` Scheduled task runs this script (refers to absolute path). The script creates routes to networks, which are achievable via the VPN-interface. Don't rename or move it!
3. `vpn-conn-update-task-template.xml` - XML representation of task. Tasks in Windows could be imported or exported with XML. This is the skeleton of task to be scheduled. Script `vpn-create.ps1` creates `vpn-conn-update-task.xml` and imports it to *Task Scheduller*.
5. `pressAnyKeyToExit.psm1` - PowerShell module. It's name says for itself.


## To do

3. redirect output of `schtasks` to PS console.



## Widespread common VPN connection issues on Windows

Creating VPN connections on Windows user/administrator may encounter few little but annoying widely known issues:

1. Internet does not work after VPN connection established.
2. Internet works slower after VPN connection established.
3. Some sites says that you connect from unusual place and suggests to recognize captcha or other.
4. Could not see and ping computers in destination network.

The first 3 items could happen because of you connect to Internet through VPN.  
**Solution for 1,2,3.** Go to Network Connections **->** right click on VPN connection **->** Properties **->** Network **->** IP version 4 **->** click button Properties **->** click button Advanced **->** deselect Use basic gateway in remote network. May you need to make the same action for IP version 6.  
Another way is to run PowerShell command
```
PS C:\> Set-VpnConnection -Name $vpn_name -SplitTunneling $True
```
where `$vpn_name` is name of the VPN connection.

**Solution for 4.** To solve problem described in item 4, additional routes must be registered every time after VPN-connection established. In `cmd`
```
C:\> route add 192.168.88.0 mask 255.255.255.0 10.100.100.254
```
where `192.168.88.0` - destination network or host, `255.255.255.0` - network mask, `10.100.100.254` - VPN gateway. `-p` option could used to save route permanently. It is useful for desktops and not recommended if your computer is not stationary (notebook).