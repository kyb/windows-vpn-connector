
#check if not run from Administrator
#If (-NOT ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator"))

# Rerun from admin. Without Args
#if (!([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")) { Start-Process powershell.exe "-NoProfile -ExecutionPolicy Bypass -File `"$PSCommandPath`"" -Verb RunAs; exit }

# Rerun from Administrator. With args
#If(-NOT ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator") ){   
#	$arguments = "& '" + $myinvocation.mycommand.definition + "'"
#	Start-Process powershell -Verb runAs -ArgumentList $arguments
#	Break
#}


# Allow scripts execution for current user
Set-ExecutionPolicy RemoteSigned -Scope CurrentUser #-Confirm


function Get-ScriptDirectory {
    Split-Path -Parent $PSCommandPath
}

$script_name = Split-Path -Leaf $PSScriptRoot
$install_path = "$env:appdata\kyb-script\$script_name"

## Скопировать файлы
#Copy-Item $PSScriptRoot $install_path/ -recurse -force -Exclude ".git*"  ## Работает плохо
New-Item -ItemType Directory -Force -Path "$install_path"  | out-null
Get-ChildItem $PSScriptRoot -Recurse -Exclude ".git*" | Copy-Item -force -Destination {Join-Path "$install_path" $_.FullName.Substring($PSScriptRoot.length)}

$vpn_create_path = "$install_path\vpn-create.ps1"
write-host Run vpn-create.ps1 from instalation folder
write-host $vpn_create_path
#Start-Process powershell '-NoExit -NoProfile -File C:\Users\kyb\AppData\Roaming\kyb-script\windows-vpn-connector-kosmos6\vpn-create.ps1 ' -nonewwindow  #-Verb runAs 
#Start-Process powershell "-NoExit -NoProfile -ExecutionPolicy ByPass -File `"${vpn_create_path}`" " -Verb runAs  #-nonewwindow #
Start-Process powershell "-NoProfile -ExecutionPolicy ByPass -File `"${vpn_create_path}`" " -Verb runAs  #-nonewwindow #
#"-NoProfile -ExecutionPolicy Bypass -File `"$PSCommandPath`""


#pause
Import-Module $PSScriptRoot/pressAnyKeyToExit
pressAnyKeyToExit
