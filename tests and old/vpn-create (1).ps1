# VPN Connection provisioning 
$vpn_name = "VPN-Kosmos6"
$vpn_server = "sledge.triolan.com.ua"
$target_nets =  "192.168.99.0/24, 192.168.43.0/24"

#------------------------------------------------------------
# VPN Connection look-up to check any previous installations
#------------------------------------------------------------
$vpnConnections = Get-VpnConnection #-AllUserConnection

if($vpnConnections.Name -eq $vpn_name)
{
    Write-Host $vpn_name connection is already configured on your system. -ForegroundColor Yellow
    Write-Host "If you wish to reinstall, please uninstall the connection and then attemp again." -ForegroundColor Yellow
    $x = read-host "Do you wish to uninstall $vpn_name (Y or N)"
	if( $x[0] -eq "Y" ){ rasdial $vpn_name /disconnect; Remove-VpnConnection -Name $vpn_name -Force  }
	else { exit }
}

Write-Host Installing $vpn_name connection.

try
{
    ## Create the VPN connection �My VPN� with the EAP configuration XML generated above
    Add-VpnConnection -Name $vpn_name -ServerAddress $vpn_server -EncryptionLevel Required -TunnelType PPTP #-AuthenticationMethod MSChapV2 -SplitTunneling $True
	
	## -SplitTunneling $True - Don't use as Internet adapter. Make it True if you have internet from another adapter.
	## -RememberCredential $True - Remeber login/password after first enter
	Set-VpnConnection -Name $vpn_name -SplitTunneling $True -RememberCredential $True
}
catch
{
    Write-Host "Error in connection setup!" -ForegroundColor Red -BackgroundColor Black
    Write-Host $_.Exception.Message
    throw
}

Write-Host $vpn_name connection is ready for use.


##------------------------------------------------------------
## Schedule task on VPN connection established
##------------------------------------------------------------
#$taskName = "$vpn_name Connection Update"
#$taskRun = "Powershell.exe -command $PSScriptRoot\vpn-route.ps1"
## \todo ������ "������ ��� ������� �� �����������"
#&("$PSScriptRoot\sudo.ps1") schtasks /create /F /TN "$vpn_name Connection Update" /TR "Powershell.exe -noProfile -NonInteractive -command '$PSScriptRoot\vpn-route.ps1 $vpn_name -TargetNets $target_nets'" /SC ONEVENT /EC Application /MO "*[System[(Level=4 or Level=0) and (EventID=20225)]] and *[EventData[Data='$vpn_name']]" /RL HIGHEST #/RU �������������
$action = New-ScheduledTaskAction �Execute "Powershell.exe -noProfile -NonInteractive -command '.\vpn-route.ps1 $vpn_name -TargetNets $target_nets'" -WorkingDirectory $PSScriptRoot
$trigger = New-ScheduledTaskTrigger -AtLogon
#$principal = "Contoso\Administrator"
$settings = New-ScheduledTaskSettingsSet
$task = New-ScheduledTask -Action $action -Trigger $trigger -Settings $settings #-Principal $principal 

[xml]$task_xml = Get-Content $PSScriptRoot\vpn-conn-update-task.xml
$task_xml.FirstChild
Register-ScheduledTask -TaskName "$vpn_name Connection Update" -Force -Xml $task_xml

Write-Host "Add route task scheduled."


Write-Host "Done. Press any key to exit... "; $null = $host.ui.RawUI.ReadKey("NoEcho,IncludeKeyDown")
