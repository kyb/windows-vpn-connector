Import-Module ./pressAnyKeyToExit

$vpnName = $args[0]
if( !$vpnName ){ 
	#$vpnName="VPN-Kosmos6" 
	write-host "First arg must be VPN adapter name. " -NoNewLine; pressAnyKeyToExit
}

# Route IP address
$ips = @("192.168.43.0", "192.168.99.0")
# Route IP4 mask, TODO masks
$mask = "255.255.255.0"

$nics = [System.Net.NetworkInformation.NetworkInterface]::GetAllNetworkInterfaces() | where-object -property name -eq $vpnName
if(!$nics){
	write-host "No adapter found. " -NoNewLine; pressAnyKeyToExit
	exit
}
$adapter = $nics[0]
$VpnGateway = $adapter.GetIPProperties().UnicastAddresses[0].Address


# Delete routes if existing
foreach($ip in $ips) {
	$hasRoute = route print | findstr $ip
	if($hasRoute) {
		"Deleting route " + $ip
		route delete $ip
	}
}
# Add whatever routes we need
foreach($ip in $ips) {
	"Adding route " + $ip
	route add $ip mask $mask $VpnGateway metric 1 $adapter.InterfaceIndex
}

#write-host "Done. Press any key to exit... "; $null = $host.ui.RawUI.ReadKey("NoEcho,IncludeKeyDown")
#exit
