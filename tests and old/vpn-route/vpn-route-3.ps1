Param(
	[string]$ArgAdapterName
	[NetParams[]]$ArgTargetNets
)

Import-Module ./pressAnyKeyToExit

class NetParams
{
    [string]$ip
    [string]$mask
	function toString(){
		return "ip: $ip, mask: $mask"
	}
}

function VpnRoute( [string]$AdapterName, [NetParams[]]$targetNets ) 
{
	$AdapterName
	"targetNets"
	$targetNets
	
	
	if( !$AdapterName ){ 
		write-host "First arg must be VPN adapter name. " -NoNewLine; pressAnyKeyToExit
		#exit
	}

	$target_nets =  @( @{ip='192.168.99.0'; mask='255.255.255.0'}, \
					@{ip='192.168.43.0'; mask='255.255.255.0'} )

	$nics = [System.Net.NetworkInformation.NetworkInterface]::GetAllNetworkInterfaces() | where-object -property name -eq $AdapterName
	if(!$nics){
		write-host "No adapter found. " -NoNewLine; pressAnyKeyToExit
		#exit
	}
	$adapter = $nics[0]
	$adapter
	$VpnGateway = $adapter.GetIPProperties().UnicastAddresses[0].Address


	# Delete routes if existing
#	foreach($ip in $ips) {
#		$hasRoute = route print | findstr $ip
#		if($hasRoute) {
#			"Deleting route " + $ip
#			route delete $ip
#		}
#	}
#	# Add whatever routes we need
#	foreach($ip in $ips) {
#		"Adding route " + $ip
#		route add $ip mask $mask $VpnGateway metric 1 $adapter.InterfaceIndex
#	}

	write-host "Done. " -NoNewLine; pressAnyKeyToExit; exit
}



&VpnRoute( $ArgAdapterName )
