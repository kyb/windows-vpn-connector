# ---------------------------------------------------------------------------------------
# Modified version: sledge.com
# by Kyb Sledgehammer 2015.11.01
# ---------------------------------------------------------------------------------------
# Modified version: https://simpleverse.wordpress.com/2010/10/06/add-routes-on-vpn-connect-with-powershell-and-task-scheduler
# by Luka Ferlez @ 6.10.2010
# ---------------------------------------------------------------------------------------
# Initial version: http://www.webboise.com/windows-powershell-script-for-adding-ip-routes-across-a-vpn/
# by Chris @ 30.9.2008
# ---------------------------------------------------------------------------------------
#
#
# Add IP routes across a VPN via a DHCP assigned IP address
#
# Configuration
# ---------------------------------------------------------------------------------------
# Route IP address
$ips = @("192.168.10.0", "192.168.11.0", "192.168.23.0")
$mask = "255.255.255.0"
# VPN connection IP
$vpnIP = "192.168.43."
#TODO $vpnName = "VPN-Kosmos6"

# ---------------------------------------------------------------------------------------
#
# Get the IP address of the VPN connection
$vpnip = ipconfig | findstr $vpnIP
# ---------------------------------------------------------------------------------------
#
# If we don't have an IP address on the VPN, error and quit
if (!$vpnip) {
	"You do not have an IP address on the VPN"
	Read-Host
	exit
}
# Trim any leading/trailing whitespace
$vpnip = $vpnip.Trim()
# ---------------------------------------------------------------------------------------
#
# Split the contents of $vpnip in to an array
$vpnip = $vpnip.Split(" ")
# ---------------------------------------------------------------------------------------
#
# Find out the depth of our IP address in the array
$bit = $vpnip.Length - 1
# ---------------------------------------------------------------------------------------
#
# Get out just our IP address on the VPN
$vpnip = $vpnip[$bit]
# ---------------------------------------------------------------------------------------
#
# Delete routes if existing
foreach($ip in $ips) {
	$hasRoute = route print | findstr $ip
	if($hasRoute) {
		"Deleting route " + $ip
		route delete $ip
	}
}
# ---------------------------------------------------------------------------------------
#
# Add whatever routes we need
foreach($ip in $ips) {
	"Adding route " + $ip
	route add $ip MASK $mask $vpnip
}

#Read-Host
exit
