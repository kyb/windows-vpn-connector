# \autor kyb
# 
# 
# 
#--------------------------------------------------------------------

[CmdletBinding()]
Param(
	[Parameter(Mandatory=$True,ValueFromPipeline=$True,Position=1)]
	[string]$AdapterName,
	
	[Parameter(Mandatory=$True,ValueFromPipeline=$True)]
	[ValidateScript({
		if( $($_.ip) -match "^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$" -and `
				$($_.mask) -match "^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$" ){
			return $true
		}
	})]
	$TargetNets
)

Import-Module $PSScriptRoot/pressAnyKeyToExit


$nics = [System.Net.NetworkInformation.NetworkInterface]::GetAllNetworkInterfaces() `
		| where-object -property name -eq $AdapterName
if(!$nics){
	write-host "No adapter found. " -NoNewLine -Foreground red; pressAnyKeyToExit; throw
}
$adapter = $nics[0]
$adapter
$VpnGateway = $adapter.GetIPProperties().UnicastAddresses[0].Address


write-host "Delete routes if existing and binded to the interface. "
$ifw =  Get-NetIPInterface | where-object { $_.InterfaceAlias -eq $AdapterName }
foreach($ip in $TargetNets.ip) {
	$hasRoute = get-netroute -AssociatedIPInterface $ifw | where-object { $_.DestinationPrefix -match $ip }   #$hasRoute = route print | findstr $ip
	if($hasRoute) {
		"Deleting route " + $ip
		route delete $ip
	}
}
write-host Done.

#\todo Add-NetRoute
write-host "Add routes which are not exist yet"
foreach($target in $TargetNets) {
	$hasRoute = get-netroute | where-object { $_.DestinationPrefix -match $target.ip }
	$hasRoute
	if( !$hasRoute ){
		"Adding route $($target.ip) mask $($target.mask) $VpnGateway metric 1 if $($adapter.InterfaceIndex)"
		route add $target.ip mask $target.mask $VpnGateway metric 1 if $adapter.InterfaceIndex
	}
}
write-host Done.

write-host "Script Done. " -NoNewLine; pressAnyKeyToExit;
